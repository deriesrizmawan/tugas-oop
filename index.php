<?php

require('Animal.php');
require('Frog.php');
require('Ape.php');


$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; 
echo "Legs : " . $sheep->legs . "<br>"; 
echo "Cold Blooded : " . $sheep->cold_bloodeds . "<br><br>"; 

$katak = new Frog('Buduk');

echo "Name : " . $katak->frog . "<br>"; 
echo "Legs : " . $katak->legs . "<br>"; 
echo "Cold Blooded : " . $katak->cold_bloodeds . "<br>"; 
echo "Jump : ";  
$katak->jump();
echo "<br><br>";


$senggokong = new Ape('Kera Sakti');

echo "Name : " . $senggokong->apee . "<br>"; 
echo "Legs : " . $senggokong->legs . "<br>"; 
echo "Cold Blooded : " . $senggokong->cold_bloodeds . "<br>";
echo "Yell : "; return  $senggokong->yell();



?>